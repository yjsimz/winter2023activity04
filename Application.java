import java.util.Scanner;		//import Scanner;

public class Application		//set class;
{
	public static void main (String[] args)		//main method;
	{
		Student Sam = new Student(); 		//initializing student 1;
		
		System.out.println(Sam.getName());		//test print statement to see what we get for name object before assigning a value;
		
		Sam.attendance = 10;		//objects for student 1;
		Sam.setName("Sam");
		Sam.rScore = 31.2;
		
		Student Chris = new Student(); 		//student 2 & objects for the student (name, attendance, R score...);
		Chris.attendance = 5;
		Chris.setName("Chris");
		Chris.rScore = 23.7;
		
		System.out.println (Sam.getName() + " " + Sam.getAttendance() + " " + Sam.getRScore());
		
		System.out.println (Sam.sorryImLate());
		System.out.println (Sam.raiseHand());
		
		System.out.println (Chris.sorryImLate());
		System.out.println (Chris.raiseHand());
		
		Student[] section3 = new Student[3];
		
		section3[0] = Sam;
		section3[1] = Chris;
		
		System.out.println(section3[0].getAttendance());
		
		section3[2] = new Student();
		section3[2].setName("Ethan");
		section3[2].attendance = 10;
		section3[2].rScore = 33.7;
		
		System.out.println (section3[2].getName());
		
		System.out.println(section3[0].amountLearnt);
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Please enter the increment(amountStudied) for the amount learnt: ");
		int amountStudied = sc.nextInt();
		
		System.out.println(section3[2].learn(amountStudied));
		
		System.out.println(section3[0].learn(section3[0].attendance));
		System.out.println(section3[0].learn(-3));
		
		System.out.println(Sam.getName());
		System.out.println(Sam.getAttendance());
		System.out.println(Sam.getRScore());
		
		System.out.println(section3[1].getName());
		System.out.println(section3[1].getAttendance());
		System.out.println(section3[1].getRScore());
	}
}